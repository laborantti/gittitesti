async function timeOutFunc() {
    return "hello";
}
// const timeOutFunc = async () => "hello"; // arrow func version
timeOutFunc().then(value => console.log(value));

function timeOutFunc2() {
    return new Promise((resolve, reject) => {
        resolve("hello");
    });
}
timeOutFunc2().then(value => console.log(value));