function timeOutFunc(message) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(message + message);
        }, 1000);
    });
}
function timeOutFuncBad(message) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject(message + message);
        }, 1000);
    });
}

(async function () {
    try {
        console.log(await timeOutFuncBad("hello"));
    } catch {
        console.log("ei onnistun");
    }
    console.log(await timeOutFunc("world"));
    console.log(await timeOutFunc("here's"));
    console.log(await timeOutFunc("our"));
    const result = await timeOutFunc("message");
    console.log(result);
})(); // IIFE
