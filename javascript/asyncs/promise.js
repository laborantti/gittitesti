new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve("world");
    }, 1000);
    // reject("häh");
}).then((value) => {
    new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("niin kerta");
        }, 1000);
    }).then(value => {
        console.log(value);
    });
    console.log(value);
}).catch((value) => {
    console.log(value);
});

console.log("hello");