function timeOutFunc(message) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(message);
        }, 1000);
    });
}

timeOutFunc("world").then(value => {
    console.log(value);
    return timeOutFunc("niin kerta");
}).then(value => {
    console.log(value);
    return timeOutFunc("better");
}).then(value => {
    console.log(value);
});

console.log("hello");