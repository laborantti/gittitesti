function range(start, end)
{
	if (start > end)
	{
		const newstart = start;
		start = end;
		end = newstart;
		// start, end = end, start;
	}

}

range(3,-5);

const arr = [1, 2, 3];
console.log(arr);
[arr[0], arr[1]] = [arr[1], arr[0]];
console.log(arr);

let a = 3;
let b = 4;
[a, b] = [b, a];
console.log(a);
console.log(b);