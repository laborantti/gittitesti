interface MyType {
	value: string
}

const asyncFunction = async (url: string): Promise<MyType> => {
    const response = await fetch(url);
    const data = await response.json();
    return {
        value: data
    };
};
const immediatelyResolvedPromise = (url: string) => {
    const resultPromise = new Promise((resolve, reject) => {
        resolve(fetch(url));
    });
    return resultPromise;
};


interface Payment {
	amount: number,
	note: string
}
const angelMowersPromise = new Promise<string>((resolve, reject) => {
    setTimeout(() => {
        resolve("We finished mowing the lawn");
    }, 1000);
    reject("We couldn't mow the lawn");
});

const myPaymentPromise = new Promise<Payment>((resolve, reject) => {
    setTimeout(() => {
        resolve({
            amount: 1000,
            note: "Thank You",
        });
    }, 1000);

    reject({
        amount: 0,
        note: "Sorry lawn was not properly mowed",
    });
});

angelMowersPromise
    .then(() => myPaymentPromise.then(res => console.log(res)))
    .catch(error => console.log(error));