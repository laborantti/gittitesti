interface Fruits {
    banana: number,
    apple: number,
    mango: number,
    lemon: number,
    color?: string,
    readonly name: string
}

const fruits: Fruits = {
    banana: 118,
    apple: 85,
    mango: 200,
    lemon: 65,
    name: "fruktbomb"
};
const fruits2: Record<string, number> = {
    banana: 118,
    apple: 85,
    mango: 200,
    lemon: 65,
};

function printFruit(fruit: keyof(Fruits)) {
    console.log(fruits[fruit]);
}
printFruit("banana");