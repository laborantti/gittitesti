const countDown = (delay: number, func:() => void) => {
    setTimeout(() => {
        func();
    }, delay);
};
countDown(1000, () => console.log("terve"));

const countDown2 = (delay: number, func:(a: number) => void) => {
    setTimeout(() => {
        func(7);
    }, delay);
};
countDown2(2000, (numb) => console.log(`the lucky number is ${numb}`));